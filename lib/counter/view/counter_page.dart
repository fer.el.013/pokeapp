// Copyright (c) 2021, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokeapp/counter/counter.dart';
import 'package:pokeapp/models/pokemon.dart';
import 'package:pokeapp/repository/repository.dart';

class CounterPage extends StatelessWidget {
  const CounterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CounterCubit(),
      child: const CounterView(),
    );
  }
}

class CounterView extends StatelessWidget {
  const CounterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final count = context.select((CounterCubit cubit) => cubit.state) + 1;

    final pokemonFuture = PokeRepo().getPokemon(1);

    return FutureBuilder(
      future: pokemonFuture,
      builder: (context, AsyncSnapshot<Pokemon> snapshot) {
        if (snapshot.hasData) {
          final pokemon = snapshot.data!;
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                onPressed: () => context.read<CounterCubit>().decrement(),
                icon: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: theme.primaryColor),
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.amber,
                  ),
                  child: const Icon(Icons.arrow_left, size: 32),
                ),
              ),
              actions: [
                IconButton(
                  onPressed: () => context.read<CounterCubit>().increment(),
                  icon: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: theme.primaryColor),
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.amber,
                    ),
                    child: const Icon(Icons.arrow_right, size: 32),
                  ),
                ),
              ],
              title: Text('${pokemon.name}'.toUpperCase()),
              centerTitle: true,
            ),
            body: Center(
              child: Container(
                height: double.infinity,
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  border: Border.all(color: theme.colorScheme.onBackground),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 300,
                      child: Image.network(
                        (pokemon.sprites?.other?.home?.frontDefault).toString(),
                        height: 300,
                        fit: BoxFit.fill,
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          }
                          return Center(
                            child: Image.asset('assets/loading.gif'),
                          );
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Expanded(
                      child: ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        itemCount: pokemon.stats?.length,
                        itemBuilder: (context, index) {
                          final key = pokemon.stats?[index].stat?.name;
                          final value = pokemon.stats?[index].baseStat;
                          return ListTile(
                            title: Text(key!.toUpperCase()),
                            trailing: Text(value.toString()),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          return Center(
            child: Container(
              color: Colors.white,
              height: double.infinity,
              child: Image.asset('assets/loading.gif')),
          );
        }
      },
    );
  }
}
