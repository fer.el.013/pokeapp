// To parse this JSON data, do
//
//     final pokemon = pokemonFromJson(jsonString);

// ignore_for_file: avoid_bool_literals_in_conditional_expressions

import 'dart:convert';

Pokemon pokemonFromJson(String str) =>
    Pokemon.fromJson(json.decode(str) as Map<String, dynamic>);

String pokemonToJson(Pokemon data) => json.encode(data.toJson());

class Pokemon {
  Pokemon({
    this.id,
    this.name,
    this.baseExperience,
    this.height,
    this.isDefault,
    this.order,
    this.weight,
    this.abilities,
    this.forms,
    this.gameIndices,
    this.heldItems,
    this.locationAreaEncounters,
    this.moves,
    this.species,
    this.sprites,
    this.stats,
    this.types,
    this.pastTypes,
  });

  factory Pokemon.fromJson(Map<String, dynamic> json) => Pokemon(
        id: json['id'] != null ? json['id'] as int : 1,
        name: json['name'].toString(),
        baseExperience: json['base_experience'] != null
            ? json['base_experience'] as int
            : 1,
        height: json['height'] != null ? json['height'] as int : null,
        isDefault:
            json['is_default'] != null ? json['is_default'] as bool : false,
        order: json['order'] != null ? json['order'] as int : null,
        weight: json['weight'] != null ? json['weight'] as int : null,
        abilities: List<Ability>.from(
          (json['abilities'] as List<dynamic>).map<Ability>(
            (dynamic x) => Ability.fromJson(x as Map<String, dynamic>),
          ),
        ),
        forms: List<Species>.from(
          (json['forms'] as List<dynamic>).map<Species>(
            (dynamic x) => Species.fromJson(x as Map<String, dynamic>),
          ),
        ),
        gameIndices: List<GameIndex>.from(
          (json['game_indices'] as List<dynamic>).map<GameIndex>(
            (dynamic x) => GameIndex.fromJson(x as Map<String, dynamic>),
          ),
        ),
        heldItems: List<HeldItem>.from(
          (json['held_items'] as List<dynamic>).map<HeldItem>(
            (dynamic x) => HeldItem.fromJson(x as Map<String, dynamic>),
          ),
        ),
        locationAreaEncounters: json['location_area_encounters'] as String,
        moves: List<Move>.from(
          (json['moves'] as List<dynamic>).map<Move>(
            (dynamic x) => Move.fromJson(x as Map<String, dynamic>),
          ),
        ),
        species: Species.fromJson(json['species'] as Map<String, dynamic>),
        sprites: Sprites.fromJson(json['sprites'] as Map<String, dynamic>),
        stats: List<Stat>.from(
          (json['stats'] as List<dynamic>).map<Stat>(
            (dynamic x) => Stat.fromJson(x as Map<String, dynamic>),
          ),
        ),
        types: List<Type>.from(
          (json['types'] as List<dynamic>).map<Type>(
            (dynamic x) => Type.fromJson(x as Map<String, dynamic>),
          ),
        ),
        pastTypes: List<PastType>.from(
          (json['past_types'] as List<dynamic>).map<PastType>(
            (dynamic x) => PastType.fromJson(x as Map<String, dynamic>),
          ),
        ),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'name': name,
        'base_experience': baseExperience,
        'height': height,
        'is_default': isDefault,
        'order': order,
        'weight': weight,
        'abilities': List<dynamic>.from(
          abilities!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
        'forms': List<dynamic>.from(
          forms!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
        'game_indices': List<dynamic>.from(
          gameIndices!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
        'held_items': List<dynamic>.from(
          heldItems!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
        'location_area_encounters': locationAreaEncounters,
        'moves': List<dynamic>.from(
          moves!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
        'species': species?.toJson(),
        'sprites': sprites?.toJson(),
        'stats': List<dynamic>.from(
          stats!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
        'types': List<dynamic>.from(
          types!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
        'past_types': List<dynamic>.from(
          pastTypes!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
      };

  int? id;
  String? name;
  int? baseExperience;
  int? height;
  bool? isDefault;
  int? order;
  int? weight;
  List<Ability>? abilities;
  List<Species>? forms;
  List<GameIndex>? gameIndices;
  List<HeldItem>? heldItems;
  String? locationAreaEncounters;
  List<Move>? moves;
  Species? species;
  Sprites? sprites;
  List<Stat>? stats;
  List<Type>? types;
  List<PastType>? pastTypes;
}

class Ability {
  Ability({
    this.isHidden,
    this.slot,
    this.ability,
  });

  factory Ability.fromJson(Map<String, dynamic> json) => Ability(
        isHidden: json['is_hidden'] as bool,
        slot: json['slot'] as int,
        ability: Species.fromJson(json['ability'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'is_hidden': isHidden,
        'slot': slot,
        'ability': ability?.toJson(),
      };

  bool? isHidden;
  int? slot;
  Species? ability;
}

class Species {
  Species({
    this.name,
    this.url,
  });

  factory Species.fromJson(Map<String, dynamic> json) => Species(
        name: json['name'] as String,
        url: json['url'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'name': name,
        'url': url,
      };

  String? name;
  String? url;
}

class GameIndex {
  GameIndex({
    this.gameIndex,
    this.version,
  });

  factory GameIndex.fromJson(Map<String, dynamic> json) => GameIndex(
        gameIndex: json['game_index'] as int,
        version: Species.fromJson(json['version'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'game_index': gameIndex,
        'version': version?.toJson(),
      };

  int? gameIndex;
  Species? version;
}

class HeldItem {
  HeldItem({
    this.item,
    this.versionDetails,
  });

  factory HeldItem.fromJson(Map<String, dynamic> json) => HeldItem(
        item: Species.fromJson(json['item'] as Map<String, dynamic>),
        versionDetails: List<VersionDetail>.from(
          (json['version_details'] as List<dynamic>).map<VersionDetail>(
            (dynamic x) => VersionDetail.fromJson(x as Map<String, dynamic>),
          ),
        ),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'item': item?.toJson(),
        'version_details': List<dynamic>.from(
          versionDetails!
              .map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
      };

  Species? item;
  List<VersionDetail>? versionDetails;
}

class VersionDetail {
  VersionDetail({
    this.rarity,
    this.version,
  });

  factory VersionDetail.fromJson(Map<String, dynamic> json) => VersionDetail(
        rarity: json['rarity'] as int,
        version: Species.fromJson(json['version'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'rarity': rarity,
        'version': version?.toJson(),
      };

  int? rarity;
  Species? version;
}

class Move {
  Move({
    this.move,
    this.versionGroupDetails,
  });

  factory Move.fromJson(Map<String, dynamic> json) => Move(
        move: Species.fromJson(json['move'] as Map<String, dynamic>),
        versionGroupDetails: List<VersionGroupDetail>.from(
          (json['version_group_details'] as List<dynamic>)
              .map<VersionGroupDetail>(
            (dynamic x) =>
                VersionGroupDetail.fromJson(x as Map<String, dynamic>),
          ),
        ),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'move': move?.toJson(),
        'version_group_details': List<dynamic>.from(
          versionGroupDetails!.map<dynamic>(
            (x) => x.toJson(),
          ),
        ),
      };

  Species? move;
  List<VersionGroupDetail>? versionGroupDetails;
}

class VersionGroupDetail {
  VersionGroupDetail({
    this.levelLearnedAt,
    this.versionGroup,
    this.moveLearnMethod,
  });

  factory VersionGroupDetail.fromJson(Map<String, dynamic> json) =>
      VersionGroupDetail(
        levelLearnedAt: json['level_learned_at'] as int,
        versionGroup:
            Species.fromJson(json['version_group'] as Map<String, dynamic>),
        moveLearnMethod:
            Species.fromJson(json['move_learn_method'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'level_learned_at': levelLearnedAt,
        'version_group': versionGroup?.toJson(),
        'move_learn_method': moveLearnMethod?.toJson(),
      };

  int? levelLearnedAt;
  Species? versionGroup;
  Species? moveLearnMethod;
}

class PastType {
  PastType({
    this.generation,
    this.types,
  });

  factory PastType.fromJson(Map<String, dynamic> json) => PastType(
        generation:
            Species.fromJson(json['generation'] as Map<String, dynamic>),
        types: List<Type>.from(
          (json['types'] as List<Type>).map<Type>(
            (x) => Type.fromJson(x as Map<String, dynamic>),
          ),
        ),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'generation': generation!.toJson(),
        'types': List<dynamic>.from(
          types!.map<List<dynamic>>((x) => x.toJson() as List<dynamic>),
        ),
      };

  Species? generation;
  List<Type>? types;
}

class Type {
  Type({
    this.slot,
    this.type,
  });

  factory Type.fromJson(Map<String, dynamic> json) => Type(
        slot: json['slot'] as int,
        type: Species.fromJson(json['type'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'slot': slot,
        'type': type!.toJson(),
      };
  int? slot;
  Species? type;
}

class GenerationV {
  GenerationV({
    this.blackWhite,
  });

  factory GenerationV.fromJson(Map<String, dynamic> json) => GenerationV(
        blackWhite:
            Sprites.fromJson(json['black-white'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'black-white': blackWhite!.toJson(),
      };

  Sprites? blackWhite;
}

class GenerationIv {
  GenerationIv({
    this.diamondPearl,
    this.heartgoldSoulsilver,
    this.platinum,
  });

  factory GenerationIv.fromJson(Map<String, dynamic> json) => GenerationIv(
        diamondPearl:
            Sprites.fromJson(json['diamond-pearl'] as Map<String, dynamic>),
        heartgoldSoulsilver: Sprites.fromJson(
          json['heartgold-soulsilver'] as Map<String, dynamic>,
        ),
        platinum: Sprites.fromJson(json['platinum'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'diamond-pearl': diamondPearl!.toJson(),
        'heartgold-soulsilver': heartgoldSoulsilver!.toJson(),
        'platinum': platinum!.toJson(),
      };

  Sprites? diamondPearl;
  Sprites? heartgoldSoulsilver;
  Sprites? platinum;
}

class Versions {
  Versions({
    this.generationI,
    this.generationIi,
    this.generationIii,
    this.generationIv,
    this.generationV,
    this.generationVi,
    this.generationVii,
    this.generationViii,
  });

  factory Versions.fromJson(Map<String, dynamic> json) => Versions(
        generationI:
            GenerationI.fromJson(json['generation-i'] as Map<String, dynamic>),
        generationIi: GenerationIi.fromJson(
            json['generation-ii'] as Map<String, dynamic>),
        generationIii: GenerationIii.fromJson(
            json['generation-iii'] as Map<String, dynamic>),
        generationIv: GenerationIv.fromJson(
            json['generation-iv'] as Map<String, dynamic>),
        generationV:
            GenerationV.fromJson(json['generation-v'] as Map<String, dynamic>),
        // generationVi: GenerationVi.fromJson(
        //     json['generation-vi'] as Map<String, dynamic>),
        generationVii: GenerationVii.fromJson(
            json['generation-vii'] as Map<String, dynamic>),
        generationViii: GenerationViii.fromJson(
            json['generation-viii'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'generation-i': generationI!.toJson(),
        'generation-ii': generationIi!.toJson(),
        'generation-iii': generationIii!.toJson(),
        'generation-iv': generationIv!.toJson(),
        'generation-v': generationV!.toJson(),
        // 'generation-vi': Map.from(generationVi!)
        //     .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
        'generation-vii': generationVii!.toJson(),
        'generation-viii': generationViii!.toJson(),
      };

  GenerationI? generationI;
  GenerationIi? generationIi;
  GenerationIii? generationIii;
  GenerationIv? generationIv;
  GenerationV? generationV;
  Map<String, Home>? generationVi;
  GenerationVii? generationVii;
  GenerationViii? generationViii;
}

class Sprites {
  Sprites({
    this.backDefault,
    this.backFemale,
    this.backShiny,
    this.backShinyFemale,
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
    this.other,
    this.versions,
    this.animated,
  });

  factory Sprites.fromJson(Map<String, dynamic> json) => Sprites(
        backDefault: json['back_default'] as String,
        backFemale: json['back_female'],
        backShiny: json['back_shiny'] as String,
        backShinyFemale: json['back_shiny_female'],
        frontDefault: json['front_default'] as String,
        frontFemale: json['front_female'],
        frontShiny: json['front_shiny'] as String,
        frontShinyFemale: json['front_shiny_female'],
        other: json['other'] == null
            ? null
            : Other.fromJson(json['other'] as Map<String, dynamic>),
        versions: json['versions'] == null
            ? null
            : Versions.fromJson(json['versions'] as Map<String, dynamic>),
        animated: json['animated'] == null
            ? null
            : Sprites.fromJson(json['animated'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'back_default': backDefault,
        'back_female': backFemale,
        'back_shiny': backShiny,
        'back_shiny_female': backShinyFemale,
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale,
        'other': other == null ? null : other!.toJson(),
        'versions': versions == null ? null : versions!.toJson(),
        'animated': animated == null ? null : animated!.toJson(),
      };

  String? backDefault;
  dynamic backFemale;
  String? backShiny;
  dynamic backShinyFemale;
  String? frontDefault;
  dynamic frontFemale;
  String? frontShiny;
  dynamic frontShinyFemale;
  Other? other;
  Versions? versions;
  Sprites? animated;
}

class GenerationI {
  GenerationI({
    this.redBlue,
    this.yellow,
  });

  factory GenerationI.fromJson(Map<String, dynamic> json) => GenerationI(
        redBlue: RedBlue.fromJson(json['red-blue'] as Map<String, dynamic>),
        yellow: RedBlue.fromJson(json['yellow'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'red-blue': redBlue!.toJson(),
        'yellow': yellow!.toJson(),
      };

  RedBlue? redBlue;
  RedBlue? yellow;
}

class RedBlue {
  RedBlue({
    this.backDefault,
    this.backGray,
    this.frontDefault,
    this.frontGray,
  });

  factory RedBlue.fromJson(Map<String, dynamic> json) => RedBlue(
        backDefault: json['back_default'] != null
            ? json['back_default'] as String
            : null,
        backGray:
            json['back_gray'] != null ? json['back_gray'] as String : null,
        frontDefault: json['front_default'] != null
            ? json['front_default'] as String
            : null,
        frontGray:
            json['front_gray'] != null ? json['front_gray'] as String : null,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'back_default': backDefault,
        'back_gray': backGray,
        'front_default': frontDefault,
        'front_gray': frontGray,
      };

  String? backDefault;
  String? backGray;
  String? frontDefault;
  String? frontGray;
}

class GenerationIi {
  GenerationIi({
    this.crystal,
    this.gold,
    this.silver,
  });

  factory GenerationIi.fromJson(Map<String, dynamic> json) => GenerationIi(
        crystal: Crystal.fromJson(json['crystal'] as Map<String, dynamic>),
        gold: Crystal.fromJson(json['gold'] as Map<String, dynamic>),
        silver: Crystal.fromJson(json['silver'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'crystal': crystal!.toJson(),
        'gold': gold!.toJson(),
        'silver': silver!.toJson(),
      };

  Crystal? crystal;
  Crystal? gold;
  Crystal? silver;
}

class Crystal {
  Crystal({
    this.backDefault,
    this.backShiny,
    this.frontDefault,
    this.frontShiny,
  });

  factory Crystal.fromJson(Map<String, dynamic> json) => Crystal(
        backDefault: json['back_default'] != null
            ? json['back_default'] as String
            : null,
        backShiny:
            json['back_shiny'] != null ? json['back_shiny'] as String : null,
        frontDefault: json['front_default'] != null
            ? json['front_default'] as String
            : null,
        frontShiny:
            json['front_shiny'] != null ? json['front_shiny'] as String : null,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'back_default': backDefault,
        'back_shiny': backShiny,
        'front_default': frontDefault,
        'front_shiny': frontShiny,
      };

  String? backShiny;
  String? backDefault;
  String? frontDefault;
  String? frontShiny;
}

class GenerationIii {
  GenerationIii({
    this.emerald,
    this.fireredLeafgreen,
    this.rubySapphire,
  });

  factory GenerationIii.fromJson(Map<String, dynamic> json) => GenerationIii(
        emerald: Emerald.fromJson(json['emerald'] as Map<String, dynamic>),
        fireredLeafgreen:
            Crystal.fromJson(json['firered-leafgreen'] as Map<String, dynamic>),
        rubySapphire:
            Crystal.fromJson(json['ruby-sapphire'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'emerald': emerald!.toJson(),
        'firered-leafgreen': fireredLeafgreen!.toJson(),
        'ruby-sapphire': rubySapphire!.toJson(),
      };

  Emerald? emerald;
  Crystal? fireredLeafgreen;
  Crystal? rubySapphire;
}

class Emerald {
  Emerald({
    this.frontDefault,
    this.frontShiny,
  });

  factory Emerald.fromJson(Map<String, dynamic> json) => Emerald(
        frontDefault: json['front_default'] as String,
        frontShiny: json['front_shiny'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'front_default': frontDefault,
        'front_shiny': frontShiny,
      };

  String? frontDefault;
  String? frontShiny;
}

class Home {
  Home({
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  factory Home.fromJson(Map<String, dynamic> json) => Home(
        frontDefault: json['front_default'] as String,
        frontFemale: json['front_female'],
        frontShiny: json['front_shiny'] as String,
        frontShinyFemale: json['front_shiny_female'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale,
      };

  String? frontDefault;
  dynamic frontFemale;
  String? frontShiny;
  dynamic frontShinyFemale;
}

class GenerationVii {
  GenerationVii({
    this.icons,
    this.ultraSunUltraMoon,
  });

  factory GenerationVii.fromJson(Map<String, dynamic> json) => GenerationVii(
        icons: DreamWorld.fromJson(json['icons'] as Map<String, dynamic>),
        ultraSunUltraMoon:
            Home.fromJson(json['ultra-sun-ultra-moon'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'icons': icons!.toJson(),
        'ultra-sun-ultra-moon': ultraSunUltraMoon!.toJson(),
      };

  DreamWorld? icons;
  Home? ultraSunUltraMoon;
}

class DreamWorld {
  DreamWorld({
    this.frontDefault,
    this.frontFemale,
  });

  factory DreamWorld.fromJson(Map<String, dynamic> json) => DreamWorld(
        frontDefault: json['front_default'] as String,
        frontFemale: json['front_female'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'front_default': frontDefault,
        'front_female': frontFemale,
      };

  String? frontDefault;
  dynamic frontFemale;
}

class GenerationViii {
  GenerationViii({
    this.icons,
  });

  factory GenerationViii.fromJson(Map<String, dynamic> json) => GenerationViii(
        icons: DreamWorld.fromJson(json['icons'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'icons': icons!.toJson(),
      };

  DreamWorld? icons;
}

class Other {
  Other({
    this.dreamWorld,
    this.home,
    this.officialArtwork,
  });

  factory Other.fromJson(Map<String, dynamic> json) => Other(
        dreamWorld:
            DreamWorld.fromJson(json['dream_world'] as Map<String, dynamic>),
        home: Home.fromJson(json['home'] as Map<String, dynamic>),
        officialArtwork: OfficialArtwork.fromJson(
          json['official-artwork'] as Map<String, dynamic>,
        ),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'dream_world': dreamWorld!.toJson(),
        'home': home!.toJson(),
        'official-artwork': officialArtwork!.toJson(),
      };

  DreamWorld? dreamWorld;
  Home? home;
  OfficialArtwork? officialArtwork;
}

class OfficialArtwork {
  OfficialArtwork({
    this.frontDefault,
  });

  factory OfficialArtwork.fromJson(Map<String, dynamic> json) =>
      OfficialArtwork(
        frontDefault: json['front_default'] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'front_default': frontDefault,
      };

  String? frontDefault;
}

class Stat {
  Stat({
    this.baseStat,
    this.effort,
    this.stat,
  });

  factory Stat.fromJson(Map<String, dynamic> json) => Stat(
        baseStat: json['base_stat'] as int,
        effort: json['effort'] as int,
        stat: Species.fromJson(json['stat'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'base_stat': baseStat,
        'effort': effort,
        'stat': stat!.toJson(),
      };

  int? baseStat;
  int? effort;
  Species? stat;
}
